# bayprox
Python package to estimate sedimentary proxy records from measured data.

This package implements the ideas presented in Goswami et al. (2014) to
estimate proxy records along with associated uncertainty. The package is
called "BayProx", an abbreviation of Bayesian Proxy record estimation.
It is divided into three modules:
    1. data.py:
        Contains Python class objects to store the measured age-depth
        and proxy-depth data along with relevant metadata.
    2. agedepth.py:
        Estimates the age-depth relation for the archive based on
        depth-spanning weight functions (DWFs).
    3. proxyrecord.py:
        Estimates the proxy-record as a sequence of time order
        probability density functions.

# Installation

1. Simply clone this repository and copy ``bayprox.py`` in the directory
   where you are working in.

2. Alternatively, you can clone this repository to a desired directory
   of your choice and then create and Python startup file
   ``.pythonstartup`` in your home directory as:
```python
import sys
import os
home = os.path.expanduser("~")
sys.path.append(home + "/<path_to_package>")
```
Save the file, and that's it! From then on, all python shells should be
able to detect (and import) the ``bpl`` package without any problem.

# Documentation

COMING SOON

# Examples

An example script ``example.py`` is included in the repository. 

```bash
python3 example.py -g preremvar -v      # optimal params for regression
python3 examply.py -g proxyrecord -v    # estimates proxy PDFs
```

# License

Copyright (c) 2016 Bedartha Goswami

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301,USA.

# Author

Bedartha Goswami <goswami@pik-potsdam.de>

# About this file

Created: Fri Jul 19, 2019  12:44pm

Last modified: Fri Jul 19, 2019  04:48pm



