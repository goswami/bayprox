#! /usr/bin/env python3
"""
Demonstrates how to use the BayProx package using Heshang HS4 d18O record
=========================================================================

"""
# Created: Fri Jul 19, 2019  12:54pm
# Last modified: Fri Jul 19, 2019  04:55pm
# Copyright: Bedartha Goswami <goswami@pik-potsdam.de>

import sys
import argparse
import numpy as np
import matplotlib.pyplot as pl
from scipy.interpolate import interp1d

from bayprox import data as dat
from bayprox import agedepth as ad
from bayprox import proxyrecord as prx

import utils

# parameters for the analysis
BAYPROX_CALAGE_SAMPLING_RES = 5
CALAGE_MIN = -51.
CALAGE_MAX = 9500.
CALAGE_ARR = np.arange(CALAGE_MIN, CALAGE_MAX, BAYPROX_CALAGE_SAMPLING_RES)
BAYPROX_MAX_DEGREE = 2
BAYPROX_SOLVER_METHOD = "pinv"
PROXY_NAME = "d18O"
BAYPROX_PROXY_SAMPLING_RES = 1000

# parameters for plotting
AXLABFS, TIKLABFS, TIKSZ = 14, 12, 8


def _get_preremvar():
    """
    Estimates the optimal prior remainder variance.
    """
    utils._printmsg("Optimal prior rem variance ... ", verb)

    # set dummy BayProx:Data:SampleInfo
    utils._printmsg("Creating BayProX:Data:SampleInfo object ...", verb)
    info = dat.SampleInfo(name="xxxx",
                          datatype="xxxx",
                          archive="Speleothem",
                          )

    # load age-depth data
    DAT = np.genfromtxt(DATPATH + "hs4_ages.csv", delimiter=",", names=True)
    xname = "Distance_cm"
    yname = "Agecorr_yrs_BP"
    ename = "Age_error_yrs_BP_2sigma"
    x = DAT[xname]
    y = DAT[yname]
    e = DAT[ename] / 2.

    # set BayProx:Data:DatingTable
    utils._printmsg("Creating BayProX:Data:DatingTable object ...", verb)
    DT = dat.DatingTable(depth=x,
                         age=y,
                         ageerror=e,
                         datingmethod="U/Th",
                         sampleinfo=info)
    DT.calibration()

    # load proxy-depth data for one case trace element series and one isotopic
    # ratio series
    DAT = np.genfromtxt(DATPATH + "hs4_d18O.csv", delimiter=",", names=True)
    x = DAT["Depth_cm"]

    # set BayProx:Data:ProxyDepth
    utils._printmsg("Creating BayProX:Data:ProxyDepth object ...", verb)
    utils._printmsg("\tfor %s ..." % PROXY_NAME, verb)
    PD = dat.ProxyDepth(depth=x,
                        proxy=np.zeros(len(x)),
                        proxyerror=0.,
                        sampleinfo=info)

    # choose the prior remainder variance of the age errors such that the
    # standard deviation and mean of the posteriors are as close to the
    # standard deviation and mean of the age measurements
    utils._printmsg("optimal prior remainder variance ...", verb)
    regression_verb =-1
    n = 100
    p = np.linspace(0., 0.20, n)
    r = np.zeros(n)
    pb = utils._progressbar_start(max_value=n, pbar_on=verb)
    for i in range(n):
        r[i] = _preremvar_optimize_residual(p[i],
                                            DT, PD,
                                            CALAGE_ARR, BAYPROX_MAX_DEGREE,
                                            BAYPROX_SOLVER_METHOD,
                                            regression_verb
                                            )
        utils._progressbar_update(pb, i)
    utils._progressbar_finish(pb)


    # save residual results as CSV file
    FOUT = DATPATH + "%s_preremvar.csv" % __file__[2:-3]
    XOUT = np.c_[p, r]
    hdr = "prior rem. var.,est. residual"
    np.savetxt(FOUT, XOUT, fmt="%.6f", delimiter=",", header=hdr)
    utils._printmsg("output saved to: %s" % FOUT, verb)

    return None


def _preremvar_optimize_residual(pre_remvar, *args):
    """
    Returns the residual for a given choice of prior remainder variance.
    """
    DT, PD, calage, max_degree, method, verb = args
    pre_remvar *= DT.ageerror.std()
    DWF = ad.DWF(DT)
    DWF.set_data(PD.depth,
                 max_degree, pre_remvar, verb,
                 cal_age_min=None, cal_age_max=None, calBP_step=None,
                 nsteps=None
                 )
    regressionfunc = DWF.set_rmagemodel([max_degree, pre_remvar, verb])
    rmagemod = DWF.get_rmagemodel(regressionfunc, PD.depth, method)
    ae, rae = DT.ageerror, rmagemod[1]
    u = ae.mean() - rae.mean()
    v = ae.std() - rae.std()
    return np.sqrt(u ** 2 + v ** 2)


def _get_proxyrecord():
    """
    Estimates the proxy record for the specified proxy.
    """

    # load the prior rem variance array and residuals and estimate the value of
    # prior rem variance at which the residual is minimised
    FIN = DATPATH + "%s_preremvar.csv" % __file__[2:-3]
    preremvar_arr, residual = np.loadtxt(FIN, delimiter=",").T
    i = np.argmin(residual)
    pre_remvar = preremvar_arr[i]

    # set BayProx:Data:SampleInfo
    utils._printmsg("Creating BayProX:Data:SampleInfo object ...", verb)
    info = dat.SampleInfo(name="HS-4 stalagmite from Heshang cave, China",
                          datatype="%s isotopic ratios" % PROXY_NAME,
                          archive="Speleothem",
                          )

    # load age-depth data
    DAT = np.genfromtxt(DATPATH + "hs4_ages.csv", delimiter=",", names=True)
    xname = "Distance_cm"
    yname = "Agecorr_yrs_BP"
    ename = "Age_error_yrs_BP_2sigma"
    x = DAT[xname]
    y = DAT[yname]
    e = DAT[ename] / 2.

    # set BayProx:Data:DatingTable
    utils._printmsg("Creating BayProX:Data:DatingTable object ...", verb)
    DT = dat.DatingTable(depth=x,
                         age=y,
                         ageerror=e,
                         datingmethod="U/Th",
                         sampleinfo=info)
    DT.calibration()

    # load proxy-depth data
    DAT = np.genfromtxt(DATPATH + "hs4_d18O.csv", delimiter=",", names=True)
    xname = "Depth_cm"
    yname = "%s_per_mil" % PROXY_NAME
    x = DAT[xname]
    y = DAT[yname]

    # set BayProx:Data:ProxyDepth
    utils._printmsg("Creating BayProX:Data:ProxyDepth object ...", verb)
    utils._printmsg("\tfor %s ..." % PROXY_NAME, verb)
    PD = dat.ProxyDepth(depth=x,
                        proxy=y,
                        proxyerror=0.,
                        sampleinfo=info)


    # set BayProx:AgeDepth:DWF
    # DWF.output contains all the relevant info
    utils._printmsg("Estimating DWFs ...", verb)
    # Fix MoTaBaR parameters
    pre_remvar *= DT.ageerror.std()
    DWF = ad.DWF(DT)
    DWF(PD.depth,
        calBP_axis=CALAGE_ARR,
        max_degree=BAYPROX_MAX_DEGREE,    # maximum degree of regression curve
        pre_remvar=pre_remvar,            # influences the variance of results
        method=BAYPROX_SOLVER_METHOD,
        verbose=int(verb)
        )

    # plot age model
    utils._printmsg("plot age model ...", verb)
    DWF_f = DWF.output["DWF_final"]
    DWF_f = (DWF_f.T / DWF_f.sum(axis=1)).T
    prx_d = DWF.proxydepth
    calbp = DWF.calbp
    fig = pl.figure(figsize=[18, 8])
    ax1 = fig.add_axes([0.075, 0.10, 0.45, 0.80])
    im = ax1.pcolormesh(prx_d, calbp, DWF_f,
                        cmap="plasma_r",
                        )
    cb = pl.colorbar(im, shrink=1.00, aspect=10, extend="max")
    cb.set_label("Scaled DWF", fontsize=AXLABFS)
    ax1.set_ylim(ax1.get_ylim()[::-1])
    rmagemod = DWF.output["RM_agemod"]
    ax2 = fig.add_axes([0.60, 0.10, 0.35, 0.80])
    ra, rae = rmagemod[0], rmagemod[1]
    pd = DWF.proxydepth
    ax2.errorbar(DT.depth, DT.age, yerr=DT.ageerror, fmt="ko")
    ax2.plot(pd, ra, "k-")
    ax2.plot(pd, ra+rae, "k:")
    ax2.plot(pd, ra-rae, "k:")
    ax2.set_ylim(ax2.get_ylim()[::-1])
    ax2.grid()
    for ax in [ax1, ax2]:
        ax.set_xlim(0., 260.)
        ax.set_ylim(CALAGE_MAX, CALAGE_MIN)
        ax.set_xlabel("Depth (mm)", fontsize=AXLABFS)
        ax.set_ylabel("Age (years BP)", fontsize=AXLABFS)
        ax.tick_params(labelsize=TIKLABFS, size=TIKSZ)
    OUT = DATPATH + "%s_hs4_%s_agemodel.png" % (__file__[2:-3], PROXY_NAME)
    fig.savefig(OUT)
    utils._printmsg("figure saved to: %s" % OUT, verb)

    # set BayProx:ProxyRecord:ProxyDistribution
    utils._printmsg("Estimating proxy distributions ...", verb)
    PDist = prx.ProxyDistributions(DWF)
    sd_mult = 3.
    limits = PDist.get_limits(DWF, PD, sd_mult)
    PDist.get_pdf(DWF, PD, res=BAYPROX_PROXY_SAMPLING_RES, limits=limits)
    cdfmat = get_cdfmat(PDist.pdfmat, PDist.proxyspan)

    # set BayProx:ProxyRecord:ProxyEstimate
    utils._printmsg("Estimating proxy mean and variance ...", verb)
    PEst= prx.ProxyEstimates()
    prx_mean= PEst.get_mean(DWF, PD)
    prx_var= PEst.get_variance(DWF, PD, mean=prx_mean)
    prx_std = np.sqrt(prx_var)
    PEst.get_variance(DWF, PD, mean=PEst.mean_value)
    utils._printmsg("Estimating proxy quantiles ...", verb)
    prx_pc05 = get_proxy_percentile(5., cdfmat, PDist.proxyspan)
    prx_pc10 = get_proxy_percentile(10., cdfmat, PDist.proxyspan)
    prx_pc25 = get_proxy_percentile(25., cdfmat, PDist.proxyspan)
    prx_pc50 = get_proxy_percentile(50., cdfmat, PDist.proxyspan)
    prx_pc75 = get_proxy_percentile(75., cdfmat, PDist.proxyspan)
    prx_pc90 = get_proxy_percentile(90., cdfmat, PDist.proxyspan)
    prx_pc95 = get_proxy_percentile(95., cdfmat, PDist.proxyspan)

    # plot proxy record
    utils._printmsg("plot proxy record ...", verb)
    fig = pl.figure(figsize=[18., 8.])
    ax1 = fig.add_axes([0.10, 0.14, 0.90, 0.80])
    PDist_pdf_scaled = PDist.pdfmat / PDist.pdfmat.sum(axis=0)
    im = ax1.pcolormesh(DWF.calbp, PDist.proxyspan, PDist.pdfmat,
                        cmap="BuGn", alpha=0.65,
                        vmin=0., vmax=1.
                        )
    cb = pl.colorbar(im)
    ax1.plot(DWF.calbp, prx_mean, "r:", lw=1.25)
    ax1.plot(DWF.calbp, prx_pc25, "k:", lw=1.25)
    ax1.plot(DWF.calbp, prx_pc50, "c:", lw=1.25)
    ax1.plot(DWF.calbp, prx_pc75, "k:", lw=1.25)
    ax1.set_ylim(ax1.get_ylim()[::-1])
    ax1.set_xlim(ax1.get_xlim()[::-1])
    ax1.set_xlabel("Time (years BP)", fontsize=AXLABFS)
    ax1.set_ylabel(r"$\delta^{18}$O (per mil VPDB)", fontsize=AXLABFS)
    cb.set_label("Scaled probability density", fontsize=AXLABFS)
    for ax in fig.axes:
        ax.tick_params(labelsize=TIKLABFS, size=TIKSZ)
    OUT = DATPATH + "%s_hs4_%s_proxyrecord.png" % (__file__[2:-3], PROXY_NAME)
    fig.savefig(OUT)
    utils._printmsg("figure saved to: %s" % OUT, verb)


    # save BayProX objects in NPZ format
    utils._printmsg("saving data in NPZ format ...", verb)
    FOUT = DATPATH + "%s_hs4_%s_proxyrecord" % (__file__[2:-3], PROXY_NAME)
    np.savez(FOUT,
             DT=DT, PD=PD, DWF=DWF, info=info,
             PDist=PDist, PEst=PEst,
             prx_mean=prx_mean, prx_var=prx_var, prx_std=prx_std,
             prx_pc25=prx_pc25, prx_pc50=prx_pc50, prx_pc75=prx_pc75,
             prx_pc10=prx_pc10, prx_pc05=prx_pc05,
             prx_pc90=prx_pc90, prx_pc95=prx_pc95,
             calage_sampling_res=BAYPROX_CALAGE_SAMPLING_RES,
             proxy_sampling_res=BAYPROX_PROXY_SAMPLING_RES,
             pre_remvar=pre_remvar, max_degree=BAYPROX_MAX_DEGREE,
             )
    utils._printmsg("saved to: %s.npz" % (DATPATH + FOUT), verb)

    # save proxy record in text format
    utils._printmsg("save proxy record as CSV ...", verb)
    X = np.c_[DWF.calbp,
              prx_mean, prx_std, prx_pc50,
              prx_pc05, prx_pc10, prx_pc25,
              prx_pc75, prx_pc90, prx_pc95
              ]
    hdr = ["Age (yBP)",
            "%s mean" % yname,
            "%s std" % yname,
            "%s median" % yname,
            "%s pc05" % yname,
            "%s pc10" % yname,
            "%s pc25" % yname,
            "%s pc75" % yname,
            "%s pc90" % yname,
            "%s pc95" % yname,
            ]
    hdr = ",".join(hdr)
    FOUT = DATPATH + __file__[2:-3] + "_hs4_%s_proxyrecord.csv" % PROXY_NAME
    np.savetxt(FOUT, X, fmt="%.6f", delimiter=",", header=hdr)
    utils._printmsg("saved to: %s" % FOUT, verb)
    return None


def get_cdfmat(pdfmat, var_span, verbose=False, pbar=False):
    """
    Returns Cumulative Distribution Functions from given PDFs.

    pdfmat.shape = (nv, nt)
    """
    pdfmat = pdfmat.T
    nt = pdfmat.shape[0]
    bj = 0.5 * np.r_[
                     var_span[1] - var_span[0],
                     var_span[2:] - var_span[:-2],
                     var_span[-1] - var_span[-2]
                    ]                               # Riemann sum width
    cdfmat = np.zeros(pdfmat.shape)
    for i in range(nt):
        pdf = pdfmat[i]
        cdfmat[i] = np.cumsum(pdf * bj)

    return cdfmat.T


def get_proxy_percentile(pc, cdfmat, var_span, verbose=False):
    """
    Returns the Inter-Quartile Range for the paleo dataset.

    cdfmat.shape = (nv, nt)
    """
    cdfmat = cdfmat.T
    nt = cdfmat.shape[0]
    q = np.zeros(nt)
    for i in range(nt):
        q[i] = np.interp(pc / 100., cdfmat[i], var_span)
    return q


def _parse_args():
    """Parse input arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-g", "--get",
                        choices=[
                                 "preremvar",
                                 "proxyrecord"
                                 ],
                        type=str,
                        help="Analysis to execute")
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        help="Print messages and progress bar")
    return parser.parse_args()


if __name__ == "__main__":
    DATPATH = "./bayprox/other/datasets/heshang/"
    args = _parse_args()
    verb = args.verbose
    _func = eval("_get_%s" % args.get)
    _func()
